// TODO: Add Shapes (Rectangle/Square, Circle/Ellipse, RoundedRect, regular n-gon, Polygon)
// TODO: Add Paths (multiple connected line segments, bezier, quadratic, arc)
// TODO: Add Point/Pixel
// TODO: Add Images
// TODO: Add z-ordering (numeric, above, below)
// TODO: Transformations
// TODO: More protocol infrastructure (e.g., Object.isOfType(Protocol))
//       mixin method creates mapping, because no strong typing
//       Maybe this should be separate library
// TODO: Gradients (Linear, Radial)
// TODO: Text
// TODO: Effects (shadows, blurs)
// TODO: CSG/Compositing/Masking
// TODO: Smart drawing to small canvas, then composite to main canvas
// TODO: Crappy shaders

var r2 = function(context) {
    this.context = context
}

//
// Shape Group class
// Implements Drawable, Transformable, Cloneable
function ShapeGroup(tag)
{
    //
    // tag - an identifier
    //
    this.tag =  tag;
    this.drawables = [];
    this.context = r2.context;
    this.transformation = Transformation.identity();
    this.draw = function()
    {
        for(var shape in shapes)
        {
            transformedShape = shape.transformed(this.transformation)
            transformedShape.draw()
        }
    }


    // Constructor
    {

    }
}

// Transformable Protocol
// A protocol that objects which can be rotated, translated, or scaled
// can implement
function Transformable()
{

    // Required
    // Returns a new object that has transformation applied to it
    this.transformed = function(transformation){}

    // Applies transformation to the current object
    this.transform = function(transformation){}
}

// Drawable Protocol
// A protocol for objects that can draw themselves into a context
function Drawable() {
    // Required
    this.draw = function(context){}

    // Optional
    this.origin = null;
    this.stroke = null;
    this.strokeColor = "#000000";
    this.fillColor = "#FFFFFF";

}

// Cloneable Protocol
// A protocol for objects that can be cloned
function Cloneable() {
    // Required
    this.clone = function(){}
}

// Shape protocol
// A protocol for objects that are a closed 2d shape
function Shape2d()
{
    this.perimeter = function{}
    this.area = function(){}
}

//

// Class representing 2d Matrix transformations
function Transformation2d()
{
    // TODO: Add matrices for transformation
    // TODO: Add methods for creating new transformations
    //       like rotate(rad), translate(x, y), scale(x, y),
    //       and skew(x, y) that return new transformations
    //       that are the current transform with the new
    //       transformation applied, so transformations can
    //       be chained.
    //       e.g., Transformation2d.identity.rotate(Math.PI/2).translate(3, 4).scale(2.0, 2.0)

    this.transform = function(x, y)
    {
        // apply transformation to x, y points
    }
}

// returns 2d identity transformation
// which is really just a Transformation with the
// default initialization
Transformation2d.prototype.identity = function()
{
    return Transformation2d()
}

